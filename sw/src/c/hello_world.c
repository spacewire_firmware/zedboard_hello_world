//
// File .......... hello_world.c
// Author ........ Steve Haywood
// Version ....... 1.0
// Date .......... 27 December 2021
// Description ...
//   The classic hello world C application.
//


#include <stdio.h>
#include "xil_printf.h"


int main()
{
  print("Hello World\n\r");
  return 0;
}
